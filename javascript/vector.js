// Declare the Vector object
var Vector = (function() {
    // Getters and setters for the X and Y values.
    Vector.prototype.setX = function(pX){
        this.mX = pX;
    };
    Vector.prototype.setY = function(pY){
        this.mY = pY;
    };
    Vector.prototype.getX = function(){
        return this.mX;
    };
    Vector.prototype.getY = function(){
        return this.mY;
    };
    Vector.prototype.add = function(pVector){
        this.setX(this.getX() + pVector.getX());
        this.setY(this.getY() + pVector.getY());
    };
    Vector.prototype.subtract = function (pVector) {
        this.setX(this.getX() - pVector.getX());
        this.setY(this.getY() - pVector.getY());
    };
    Vector.prototype.magnitude = function(){
        // the square root of x squared plus y squared.
        return Math.sqrt((Math.pow(this.getX(), 2) + Math.pow(this.getY(), 2)));
    };
    Vector.prototype.divide = function(pScalar){
        this.setX(this.getX() / pScalar);
        this.setY(this.getY() / pScalar);
    };
    Vector.prototype.multiply = function(pScalar){
        this.setX(this.getX() * pScalar);
        this.setY(this.getY() * pScalar);
    };
    Vector.prototype.normalise = function(){
        return this.divide(this.magnitude());
    };
    Vector.prototype.limitTo = function(pLimit){
        return this.normalise().multiply(pLimit);
    };
    Vector.prototype.copy = function(){
        return new Vector(this.getX(), this.getY());
    };
    Vector.prototype.dotProduct = function(mVector){
        return ((this.getX() * mVector.getX()) + (this.getY() * mVector.getY()));
    };
    Vector.prototype.angleBetween = function(mVector){
        return Math.acos(this.dotProduct(mVector) / (this.magnitude() * mVector.magnitude()));
    };
    // Constructor
    function Vector(pX, pY){
        this.setX(pX);
        this.setY(pY)
    };

    // JavaScript objects need to return themselves.
    return Vector;

})();