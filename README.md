# README #

This is my very simple, but pretty effective JavaScript vector library. It defines a two dimensional vector object and some very useful functions.

## Usage ##
This library is meant to be used with a HTML5 Canvas object. Declaring a vector in your existing projects is very simple. Include the file in your project and call the vector object, for example:

```
#!javascript

var v = new Vector(0, 0);
```
This object now represents a point on your canvas (0, 0) and you may now apply whichever functions that are included in this library (or write your own) to that object. I've tried to make the functions as self explanatory as possible.